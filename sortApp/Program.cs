﻿using System;
using System.Text.RegularExpressions;
using Sort;
namespace sortApp
{
    class Program
    {
        static void Main(string[] args)

        {
            SortClass sc = new SortClass();
            Console.WriteLine("Enter 6 unique digits separated by \",\"");
            string temp = Console.ReadLine();
            if(!Regex.IsMatch(temp, "^[,0-9]+$"))
            {
                Console.WriteLine("String should consist of 6 unique digits separated by \",\"");
                throw new StringValidation("String should consist of 6 unique digits separated by \",\"");
            }
            else
            {
                int[] data = Array.ConvertAll<string, int>(temp.Split(','), int.Parse);
                data = sc.Sort(data);
                Console.WriteLine("["+string.Join(",", data)+"]");
            }
            Console.ReadKey();
        }
    }
}
