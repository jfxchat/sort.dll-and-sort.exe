﻿using System;


namespace Sort
{
    public class StringValidation : Exception
    {
        public StringValidation(string message)
        {

        }

    }
    public class SortClass
    {
        public int[] Sort(int[] data)
        {
            if (data.Length != 6)
                throw new StringValidation("Array.Length must be 6");
            int count = 0;
            bool ch = true;
            string step_info = "";
            while (ch && count < 3)
            {
                ch = false;
                count++;
                
                for (int i = 0; i < 3; i++)
                {
                    
                    if (data[i] > data[i + 3])
                    {
                        step_info += string.Format("{0}{1}<->{2}{3} [{4}]\n",data[i], data[i + 1], data[i + 2], data[i + 3],string.Join(",",data)); 
                        ch = true;
                        /*Temp data*/
                        int a = data[i];
                        int b = data[i + 1];
                        int c = data[i + 2];
                        int d = data[i + 3];
                        /*End temp data*/

                        data[i] = c;
                        data[i + 2] = a;
                        data[i + 1] = d;
                        data[i + 3] = b;
  

                    }
                    

                }
            }
            Console.WriteLine(step_info);
            return data;
        }
    }
}
